import Foundation

public enum URLS {
    static public func albums(_ request: String, limit: Int = 25) -> String {
        let term = request.reduce(""){ $1 == " " ? "\($0)+" : "\($0)\($1)"}
        return "https://itunes.apple.com/search?term=\(term)&entity=album&limit=\(limit)"
    }
    /// https://itunes.apple.com/lookup?entity=song&id= + collectionId
    static public let songs = "https://itunes.apple.com/lookup?entity=song&id="
}
