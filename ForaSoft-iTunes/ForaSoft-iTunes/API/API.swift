import Foundation
import Dispatch

protocol GET {
    func get(from urlString: String, _ completion: @escaping ((Data?)->()))
}

enum DispatcherQueue {
    static let updaterGroup = DispatchGroup()
    static let updaterQueue = DispatchQueue(label: "com.updater.queue", qos: .background)
}

final class API: GET {
    
    public func updater(request: String) {
        let url = URLS.albums(request)
        
        DispatcherQueue.updaterGroup.enter()
        DispatcherQueue.updaterQueue.async(group: DispatcherQueue.updaterGroup, execute: {
            API.shared.get(from: url, { [self] data -> Void in
                if let data = data {
                    let decoder = Handler.jsonDecoder()
                    let semaphore = DispatchSemaphore(value: 0)
                    
                    do {
                        var request = try decoder.decode(Request.self, from: data)

                        let queue = OperationQueue()
                        queue.maxConcurrentOperationCount = 2
                        queue.qualityOfService = .userInteractive
                                                
                        for i in request.results.indices {
                            guard let imageUrl = request.results[i].artworkUrl100, let id = request.results[i].collectionId else { continue }
                            let songsUrl = URLS.songs + String(id)
                            
                            //MARK: Load album cover
                            let loadImage = BlockOperation(block: {
                                API.shared.get(from: imageUrl, { data in
                                    guard let data = data else { return }
                                    request.results[i].image100_data = data
                                })
                            })
                            loadImage.queuePriority = .veryHigh
                            loadImage.qualityOfService = .userInteractive
                            
                            //MARK: Load track list
                            let loadSongs = BlockOperation(block: {
                                API.shared.get(from: songsUrl, { data in
                                    guard let data = data else { return }
                                    do {
                                        request.results[i].album = try decoder.decode(Album.self, from: data)
                                    } catch {
                                        print(error.localizedDescription)
                                    }
                                })
                            })
                            loadSongs.queuePriority = .veryHigh
                            loadSongs.qualityOfService = .background
                            
                            
                            queue.addOperations([loadImage,loadSongs], waitUntilFinished: false)
                        }
                        queue.addBarrierBlock {
                            storage.setRequest(request)
                        }
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            })
        })
    }

    
    static public let shared = API()
    private init(){}
    
    private let storage = Storage.shared
    
    final func get(from urlString: String, _ completion: @escaping ((Data?)->())) {
        let url = URL(string: urlString)!
        let data: Data?
        
        let semaphore = DispatchSemaphore(value: 0)
        
        if let cachedNSData = storage.cache.object(forKey: url as NSURL) {
            data = Data(referencing: cachedNSData); semaphore.signal()
        } else {
            do {
                let newData = try Data(contentsOf: url, options: [.mappedRead])
                
                let nsdata: NSData = NSData(data: newData)
                storage.cache.setObject(nsdata, forKey: url as NSURL)
                
                data = newData; semaphore.signal()
            } catch {
                data = nil; semaphore.signal()
            }
        }
        semaphore.wait()
        completion(data)
    }
}
