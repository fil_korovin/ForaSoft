import Foundation

final class Storage {
    
    static public let shared = Storage()
    private init() {}
    
    public let cache: NSCache<NSURL,NSData> = .init()
    
    private(set) public var request: Request!
    private(set) public var results: [Result] = .init()
    
    public func setRequest(_ request: Request) {
        self.request = request
        
        if !results.isEmpty {
            self.results = request.results
        } else {
            self.results.append(contentsOf: request.results)
        }
        DispatcherQueue.updaterGroup.leave()
    }
}
