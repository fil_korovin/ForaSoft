import UIKit

class NavigationController: UINavigationController {
    
    private(set) open var VC1_RVC: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //MARK: -- ViewControllers
        VC1_RVC = ResultsViewController()
        
        //MARK: -- Setting ViewControllers for UINavigationController
        super.viewControllers = [VC1_RVC]        
    }
}
