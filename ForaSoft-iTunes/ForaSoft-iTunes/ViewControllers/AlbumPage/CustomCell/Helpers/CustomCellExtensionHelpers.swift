import UIKit

extension CustomCell {
    static public var id: String { String(describing: self )}
    static public var nib: UINib { UINib(nibName: id, bundle: nil )}
}
