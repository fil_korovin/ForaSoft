import UIKit

class CustomCell: UITableViewCell {
    
    @IBOutlet weak var labelTrackNumber: UILabel!
    @IBOutlet weak var labelTrackName: UILabel!
    
    public func setTrackInfo(_ track: Track) {
        guard let number = track.trackNumber else { return }
        self.labelTrackNumber.text = String(number) + "."
    }
    //    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    //        super.init(style: style, reuseIdentifier: reuseIdentifier)
    //    }
    //
    //    required init?(coder aDecoder: NSCoder) {
    //        fatalError("init(coder:) has not been implemented")
    //    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
