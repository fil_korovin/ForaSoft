import UIKit

extension AlbumPage {
    static let trackCellId: String = { CustomCell.id }()
    static let trackCellNib: UINib = { CustomCell.nib }()
}
