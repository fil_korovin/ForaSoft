import UIKit

final class AlbumPage: UIViewController {
    
    @IBOutlet weak var albumCover: UIImageView!
    @IBOutlet weak var bgAlbumCover: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    
    private(set) public var result: Result!
    
    public func setResult(_ result: Result) {
        self.result = result
        albumCover.image = UIImage(data: result.image100_data!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        let trackCell = (Self.trackCellNib, Self.trackCellId)
        tableView.register(trackCell.0, forCellReuseIdentifier: trackCell.1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("QWEQWE")
        self.tableView.reloadData()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
