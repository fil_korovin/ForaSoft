import UIKit

extension AlbumPage: UITableViewDelegate, UITableViewDataSource {
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return result.album.trackList.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let track = result.album?.trackList[indexPath.row],
              let trackCell = tableView.dequeueReusableCell(withIdentifier: Self.trackCellId, for: indexPath) as? CustomCell
        else { fatalError() }
        trackCell.setTrackInfo(track)
        return trackCell
    }
}
