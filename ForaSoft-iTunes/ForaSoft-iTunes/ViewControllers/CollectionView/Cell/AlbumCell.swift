import UIKit

final class AlbumCell: UICollectionViewCell {
    static var id: String { String(describing: self )}
    static var nib: UINib { UINib(nibName: id, bundle: nil )}
    
    @IBOutlet weak var imageView_album_cover: UIImageView!
    
    @IBOutlet weak var view_artist_name: UIView!
    @IBOutlet weak var label_artist_name: UILabel!
    
    @IBOutlet weak var view_album_title: UIView!
    @IBOutlet weak var label_album_title: UILabel!
    
    public func set(_ result: Result) {
        guard let imageData = result.image100_data else { return }
        self.imageView_album_cover.image = UIImage(data: imageData) ?? UIImage(named: "albumcover_placeholder")
        
        label_artist_name.text = result.artistName
        label_album_title.text = result.collectionName
    }
    
    fileprivate func view_album_image_config() {
        imageView_album_cover.layoutIfNeeded()
        imageView_album_cover.layer.borderWidth = 1
        imageView_album_cover.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        imageView_album_cover.cornerRadius()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.cornerRadius()
        view_album_image_config()
    }
}

extension AlbumCell {
    internal func shadow(_ view_bgColor: UIColor = .systemBackground) {
        backgroundColor = view_bgColor
        layer.masksToBounds = false
        layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 0.5)
        layer.shadowOffset = .zero
        layer.shadowRadius = 3.5
        layer.shadowOpacity = 1.0
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: contentView.layer.cornerRadius).cgPath
    }
}

extension UIView {
    func cornerRadius(_ factor: CGFloat = 10.0) {
        let h = bounds.height
        let w = bounds.width
        layer.cornerRadius = (h/w) * factor
    }
}
