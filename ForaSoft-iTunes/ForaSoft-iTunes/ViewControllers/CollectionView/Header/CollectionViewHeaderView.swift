import UIKit

class CollectionViewHeaderView: UICollectionReusableView {
    
    @IBOutlet var searchBar: UISearchBar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        searchBar.delegate = self
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        // self.searchBar = UISearchBar()
        // self.addSubview(searchBar)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension CollectionViewHeaderView: UISearchBarDelegate {
    private func setup() {
        
    }
}
