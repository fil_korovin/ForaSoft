import UIKit

final class ResultsViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private let storage: Storage = Storage.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("START", Date())
        API.shared.updater(request: "Queen")
        
        //MARK: Delegate & DataSource
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.alwaysBounceVertical = true
        collectionView.isPrefetchingEnabled = true
        
        //MARK: Register Block
        collectionView.register(Self.headerNib, forSupplementaryViewOfKind: Self.elementKindSectionHeader, withReuseIdentifier: Self.headerViewIdentifier)
        collectionView.register(Self.albumCellNib, forCellWithReuseIdentifier: Self.albumCellIdentifier)
        
        //MARK: Navigation Controller
        if let navController = self.navigationController {
            navController.isNavigationBarHidden = true
            navController.navigationBar.isHidden = true
        }
        //MARK: CollectionView Flow Layout
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let w = collectionView.bounds.width / 2.25
            let h = collectionView.bounds.height / 3.75
            let size = CGSize(width: w, height: h)
            flowLayout.itemSize = size
            
            flowLayout.scrollDirection = .vertical
            flowLayout.sectionHeadersPinToVisibleBounds = false
        }
        
        view.backgroundColor = .systemBackground
        collectionView.backgroundColor = view.backgroundColor
        
        // collectionView.setNeedsLayout()
        // collectionView.layoutIfNeeded()
        
        DispatcherQueue.updaterGroup.notify(queue: .main, execute: { // [self] in
            print("END", Date())
            self.collectionView.reloadData()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let result: Result = storage.results[indexPath.row]
        
        if let album = UINib(nibName: "AlbumPage", bundle: Bundle(for: AlbumPage.self)).instantiate(withOwner: nil, options: nil)[0] as? AlbumPage {
            album.setResult(result)
            navigationController?.pushViewController(album, animated: true)
            // present(album, animated: true, completion: nil)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case Self.elementKindSectionHeader:
            let id: String = Self.headerViewIdentifier
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: id, for: indexPath) as! CollectionViewHeaderView
            return headerView
        default:
            return UICollectionReusableView()
        }
    }
    
    //MARK: -- UICollectionView config:
    
    //MARK: Header
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let width: CGFloat = collectionView.bounds.width
        let height: CGFloat = 56 // UISearchBar
        let size: CGSize = CGSize(width: width, height: height)
        return size
    }
    
    //MARK: Sections
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    //MARK: Items in section
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return storage.results.count
    }
    //MARK: Cell config
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Self.albumCellIdentifier, for: indexPath) as! AlbumCell
        cell.set(storage.results[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
    }
    
    //MARK: -- Initialization
    init() {
        // Fix: initialized with a non-nil layout parameter
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
    }
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
    }
}

extension ResultsViewController {
    //MARK: Header
    static let elementKindSectionHeader: String = {
        return UICollectionView.elementKindSectionHeader
    }()
    static let headerViewIdentifier: String = {
        return "CollectionViewHeaderView"
    }()
    static let headerNib: UINib = {
        return UINib(nibName: "CollectionViewHeaderView", bundle: Bundle(for: CollectionViewHeaderView.self))
    }()
    //MARK: Body
    static let albumCellNib: UINib = {
        return UINib(nibName: AlbumCell.id, bundle: Bundle(for: CollectionViewHeaderView.self))
    }()
    static let albumCellIdentifier: String = {
        return AlbumCell.id
    }()
}


























// MARK: UICollectionViewDelegate

/*
 // Uncomment this method to specify if the specified item should be highlighted during tracking
 override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
 return true
 }
 */

/*
 // Uncomment this method to specify if the specified item should be selected
 override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
 return true
 }
 */

/*
 // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
 override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
 return false
 }
 
 override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
 return false
 }
 
 override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
 
 }
 */
