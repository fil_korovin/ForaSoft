import Foundation

final class Handler {
    final func json<T: Codable>(_ data: Data) -> T {
        do {
            return try Self.jsonDecoder().decode(T.self, from: data)
        } catch {
            fatalError("Couldn't parse as \(T.self):\n\(error)")
        }
    }
}
