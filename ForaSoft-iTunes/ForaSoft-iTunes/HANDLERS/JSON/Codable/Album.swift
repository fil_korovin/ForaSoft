import Foundation

public struct Album: Codable {
    
    public let resultCount: Int!
    public let trackList: [Track]!
    
    private enum CodingKeys: String, CodingKey {
        case resultCount = "resultCount"
        case trackList = "results"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        resultCount = try values.decodeIfPresent(Int.self, forKey: .resultCount) ?? 0
        trackList = try values.decodeIfPresent([Track].self, forKey: .trackList) ?? .init()
    }
}
