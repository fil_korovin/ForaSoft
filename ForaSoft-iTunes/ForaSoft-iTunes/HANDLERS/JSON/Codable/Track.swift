import Foundation

public struct Track: Codable {
    public let amgArtistId: Int!
    public let artistId: Int!
    public let artistName: String!
    public let artistViewUrl: String!
    public let artworkUrl100: String!
    public let artworkUrl30: String!
    public let artworkUrl60: String!
    public let collectionArtistId: Int!
    public let collectionArtistName: String!
    public let collectionArtistViewUrl: String!
    public let collectionCensoredName: String!
    public let collectionExplicitness: String!
    public let collectionId: Int!
    public let collectionName: String!
    public let collectionPrice: Double!
    public let collectionType: String!
    public let collectionViewUrl: String!
    public let copyright: String!
    public let country: String!
    public let currency: String!
    public let discCount: Int!
    public let discNumber: Int!
    public let isStreamable: Bool!
    public let kind: String!
    public let previewUrl: String!
    public let primaryGenreName: String!
    public let releaseDate: String!
    public let trackCensoredName: String!
    public let trackCount: Int!
    public let trackExplicitness: String!
    public let trackId: Int!
    public let trackName: String!
    public let trackNumber: Int!
    public let trackPrice: Double!
    public let trackTimeMillis: Int!
    public let trackViewUrl: String!
    public let wrapperType: String!
    
    enum CodingKeys: String, CodingKey {
        case amgArtistId = "amgArtistId"
        case artistId = "artistId"
        case artistName = "artistName"
        case artistViewUrl = "artistViewUrl"
        case artworkUrl100 = "artworkUrl100"
        case artworkUrl30 = "artworkUrl30"
        case artworkUrl60 = "artworkUrl60"
        case collectionArtistId = "collectionArtistId"
        case collectionArtistName = "collectionArtistName"
        case collectionArtistViewUrl = "collectionArtistViewUrl"
        case collectionCensoredName = "collectionCensoredName"
        case collectionExplicitness = "collectionExplicitness"
        case collectionId = "collectionId"
        case collectionName = "collectionName"
        case collectionPrice = "collectionPrice"
        case collectionType = "collectionType"
        case collectionViewUrl = "collectionViewUrl"
        case copyright = "copyright"
        case country = "country"
        case currency = "currency"
        case discCount = "discCount"
        case discNumber = "discNumber"
        case isStreamable = "isStreamable"
        case kind = "kind"
        case previewUrl = "previewUrl"
        case primaryGenreName = "primaryGenreName"
        case releaseDate = "releaseDate"
        case trackCensoredName = "trackCensoredName"
        case trackCount = "trackCount"
        case trackExplicitness = "trackExplicitness"
        case trackId = "trackId"
        case trackName = "trackName"
        case trackNumber = "trackNumber"
        case trackPrice = "trackPrice"
        case trackTimeMillis = "trackTimeMillis"
        case trackViewUrl = "trackViewUrl"
        case wrapperType = "wrapperType"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        amgArtistId = try values.decodeIfPresent(Int.self, forKey: .amgArtistId)
        artistId = try values.decodeIfPresent(Int.self, forKey: .artistId)
        artistName = try values.decodeIfPresent(String.self, forKey: .artistName)
        artistViewUrl = try values.decodeIfPresent(String.self, forKey: .artistViewUrl)
        artworkUrl100 = try values.decodeIfPresent(String.self, forKey: .artworkUrl100)
        artworkUrl30 = try values.decodeIfPresent(String.self, forKey: .artworkUrl30)
        artworkUrl60 = try values.decodeIfPresent(String.self, forKey: .artworkUrl60)
        collectionArtistId = try values.decodeIfPresent(Int.self, forKey: .collectionArtistId)
        collectionArtistName = try values.decodeIfPresent(String.self, forKey: .collectionArtistName)
        collectionArtistViewUrl = try values.decodeIfPresent(String.self, forKey: .collectionArtistViewUrl)
        collectionCensoredName = try values.decodeIfPresent(String.self, forKey: .collectionCensoredName)
        collectionExplicitness = try values.decodeIfPresent(String.self, forKey: .collectionExplicitness)
        collectionId = try values.decodeIfPresent(Int.self, forKey: .collectionId)
        collectionName = try values.decodeIfPresent(String.self, forKey: .collectionName)
        collectionPrice = try values.decodeIfPresent(Double.self, forKey: .collectionPrice)
        collectionType = try values.decodeIfPresent(String.self, forKey: .collectionType)
        collectionViewUrl = try values.decodeIfPresent(String.self, forKey: .collectionViewUrl)
        copyright = try values.decodeIfPresent(String.self, forKey: .copyright)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        discCount = try values.decodeIfPresent(Int.self, forKey: .discCount)
        discNumber = try values.decodeIfPresent(Int.self, forKey: .discNumber)
        isStreamable = try values.decodeIfPresent(Bool.self, forKey: .isStreamable)
        kind = try values.decodeIfPresent(String.self, forKey: .kind)
        previewUrl = try values.decodeIfPresent(String.self, forKey: .previewUrl)
        primaryGenreName = try values.decodeIfPresent(String.self, forKey: .primaryGenreName)
        releaseDate = try values.decodeIfPresent(String.self, forKey: .releaseDate)
        trackCensoredName = try values.decodeIfPresent(String.self, forKey: .trackCensoredName)
        trackCount = try values.decodeIfPresent(Int.self, forKey: .trackCount)
        trackExplicitness = try values.decodeIfPresent(String.self, forKey: .trackExplicitness)
        trackId = try values.decodeIfPresent(Int.self, forKey: .trackId)
        trackName = try values.decodeIfPresent(String.self, forKey: .trackName)
        trackNumber = try values.decodeIfPresent(Int.self, forKey: .trackNumber)
        trackPrice = try values.decodeIfPresent(Double.self, forKey: .trackPrice)
        trackTimeMillis = try values.decodeIfPresent(Int.self, forKey: .trackTimeMillis)
        trackViewUrl = try values.decodeIfPresent(String.self, forKey: .trackViewUrl)
        wrapperType = try values.decodeIfPresent(String.self, forKey: .wrapperType)
    }
}
