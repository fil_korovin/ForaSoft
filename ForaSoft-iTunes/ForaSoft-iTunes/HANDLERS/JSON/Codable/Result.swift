import Foundation

public struct Result: Codable {
    public var artistId: Int!
    public var artistName: String!
    public var artistViewUrl: String!
    public var artworkUrl60: String!
    public var artworkUrl100: String!
    public var collectionCensoredName: String!
    public var collectionExplicitness: String!
    public var collectionId: Int!
    public var collectionName: String!
    public var collectionPrice: Double!
    public var collectionType: String!
    public var collectionViewUrl: String!
    public var contentAdvisoryRating: String!
    public var copyright: String!
    public var country: String!
    public var currency: String!
    public var primaryGenreName: String!
    public var releaseDate: String!
    public var trackCount: Int!
    public var wrapperType: String!
    
    public var image100_data: Data!
    public var album: Album!
    
    private enum CodingKeys: String, CodingKey {
        case artistId = "artistId"
        case artistName = "artistName"
        case artistViewUrl = "artistViewUrl"
        case artworkUrl60 = "artworkUrl60"
        case artworkUrl100 = "artworkUrl100"
        case collectionCensoredName = "collectionCensoredName"
        case collectionExplicitness = "collectionExplicitness"
        case collectionId = "collectionId"
        case collectionName = "collectionName"
        case collectionPrice = "collectionPrice"
        case collectionType = "collectionType"
        case collectionViewUrl = "collectionViewUrl"
        case contentAdvisoryRating = "contentAdvisoryRating"
        case copyright = "copyright"
        case country = "country"
        case currency = "currency"
        case primaryGenreName = "primaryGenreName"
        case releaseDate = "releaseDate"
        case trackCount = "trackCount"
        case wrapperType = "wrapperType"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        artistId = try values.decodeIfPresent(Int.self, forKey: .artistId)
        artistName = try values.decodeIfPresent(String.self, forKey: .artistName)
        artistViewUrl = try values.decodeIfPresent(String.self, forKey: .artistViewUrl)
        artworkUrl60 = try values.decodeIfPresent(String.self, forKey: .artworkUrl60)
        artworkUrl100 = try values.decodeIfPresent(String.self, forKey: .artworkUrl100)
        collectionCensoredName = try values.decodeIfPresent(String.self, forKey: .collectionCensoredName)
        collectionExplicitness = try values.decodeIfPresent(String.self, forKey: .collectionExplicitness)
        collectionId = try values.decodeIfPresent(Int.self, forKey: .collectionId)
        collectionName = try values.decodeIfPresent(String.self, forKey: .collectionName)
        collectionPrice = try values.decodeIfPresent(Double.self, forKey: .collectionPrice)
        collectionType = try values.decodeIfPresent(String.self, forKey: .collectionType)
        collectionViewUrl = try values.decodeIfPresent(String.self, forKey: .collectionViewUrl)
        contentAdvisoryRating = try values.decodeIfPresent(String.self, forKey: .contentAdvisoryRating)
        copyright = try values.decodeIfPresent(String.self, forKey: .copyright)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        primaryGenreName = try values.decodeIfPresent(String.self, forKey: .primaryGenreName)
        releaseDate = try values.decodeIfPresent(String.self, forKey: .releaseDate)
        trackCount = try values.decodeIfPresent(Int.self, forKey: .trackCount)
        wrapperType = try values.decodeIfPresent(String.self, forKey: .wrapperType)
        
        image100_data = nil
        album = nil
    }
}
